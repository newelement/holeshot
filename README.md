# Holeshot Front-end Kit

A small front-end starter kit with build tools. Sprinkled with Elvis dust.

Copy the frameworks files over to your project folder. Node and Bower are required. Make sure it includes these files:

* gulpfile.js
* .gitignore
* .jshintignore
* .bowerrc
* bower.json
* config.json
* package.json

### config.json

Enter your config settings (optional)

	dest :  your_destination_folder_for_your_compiled_files
	assets : your_assets_folder - leave blank tot put images,css,js,videos,fonts in the root. !! You will need to manually move the src assets to this folder.
	hostname :  enter_your_server_hostname
	env : dev (options are: dev, tst, prd)

## JS Assets Build

Specify where you want to build your JS file.

```
<!-- build:js js/app.js -->
<script src="bower/jquery/dist/jquery.js"></script>
<script src="js/app.js"></script>
<!-- endbuild -->
```

## Install Dependencies

In your project folder run [sudo] **npm install** and [sudo] **bower install**.

## Gulp

Run **gulp dev** in your project folder to kickoff browsersync, jshint and sass watching.

Run **gulp build** to compile all assets and compress images


## CSS/Sass

The **src/scss** folder structure is mainly to help organize the project files. It will help other developers know where to look for certain code.

The **src/scss/_base.scss** file starts all the basic defaults including typography.

The **src/scss/style.scss** file is the main stylesheet file. Use this file to include any pages or sections in your project.

#### Pages and Sections

Please place page or page section specifc styles in the **src/scss/pages/** or **src/scss/sections** folders. Hint- if a page section is rather large or your are builind a one page website use the **src/scss/sections** folder.

#### Variables

All variables are organized in **src/scss/variables**. The main include file for all the variables is **_variables.scss**.

#### Components

Optional components are found in **src/scss/components**. You can add/remove included components in the **_components.scss** file.

#### Mixins

Place any extra mixins you need in **src/scss/mixins/_mixins.scss**. If your project requires a lot of mixins create seperate files and then include them on **_mixins.scss**.

#### Media Queries

Don't care if you want to use min-width or max-width. It really does not matter. Most of the time you will be given a desktop PSD, so whatever. You can add/edit/delete the media query settings on **src/scss/variables/_breakpoints.scss**. Please note the pixel size because Photoshop and Chrome dev tool do not care about anything else.

#### Grid

This framework uses Susy to setup the grid. The default setup can be found on **src/scss/components/_grid.scss**. The default grid is 960/12 col/20px gutter with 10px padding on the outside container. There is a companion PSD for this setup.

#### Baseline

Typography is based on the golden ratio baseline. This particular setup is: **16px font size / 1.5 (24px) line height / 24px margin bottom**. Example uasage can be found on **src/scss/_base.scss**.

## Coding Guidelines/Requests

* Use 4 or 2 space tab-indentation. It's better to read/scan.
* Use REM with fallback to PX for font sizes. There's a mixin for this on **src/scss/mixins/_mixins.scss**.
* For now use EM or % for margins. A REM fallback will add too much bloat.
* Please don't use viewport units: vmin, vmax. Not fully supported in IE. Fallbacks are not worth the overhead.
* Try to keep css nesting to max one or two levels. Anything past this is a pain to read and plus it causes high specificity.
* Stay away from high css specificity. Use specific names or helper classes if possible.
* Comments. Use comments for complicated areas in CSS/JS/HTML/PHP etc...

Sometimes bleeding edge is not worth the blood. Find a happy medium.
